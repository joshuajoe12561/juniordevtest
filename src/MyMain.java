import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class MyMain {

    public static int getLeastCost(int k,int i, int cost, int n, int X, int[] arr){

        for (int j = 0; j < k; j++) {
            if (n<=0){
                return cost;
            }
            cost=((X+1)*arr[i]) + cost;
            n=n-X;
        }

        return getLeastCost(k,i+1,cost,n,X+1, arr);
    }
    public static void main(String[] args) {
        Scanner scanner= new Scanner(System.in);
        int n = scanner.nextInt();
        int k = scanner.nextInt();
        int[] arr = new int[n];
        for (int i = 0; i < n; i++) {
            arr[i] = scanner.nextInt();
        }
        int leastCost=0;
            int x = 1;
            int total= 0;

            leastCost=getLeastCost(k,0,total,n-1,x,arr);


        System.out.println(leastCost);

    }
}
