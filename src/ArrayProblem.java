import java.util.*;
import java.util.stream.Collectors;

public class ArrayProblem {
    public static void main(String[] args) {
        Scanner scanner= new Scanner(System.in);
        System.out.println("Enter number of test cases:");
        int nT = scanner.nextInt();
        List<int[]> result = new ArrayList<>();
        for (int i = 0; i < nT; i++) {
            System.out.println("Test case "+i+" enter size of array :");
            int tSa = scanner.nextInt();
            List<Integer> arr = new ArrayList<>();

            System.out.println("Enter array inputs: ");
            for (int j = 0; j < tSa; j++) {
                arr.add(scanner.nextInt());
            }
            int[] positionArr = new int[tSa];
            int[]temp =sort(arr);
            for (int j = 0; j < arr.size(); j++) {

                positionArr[j]=findIndex(arr,temp[j]);
            }
            result.add(positionArr);
        }
        for (int[] arr:result) {
            System.out.println(Arrays.toString(arr));
        }

    }
    public static int findIndex(List<Integer> arr, int t)
    {
        if (arr == null) {
            return -1;
        }

        int len = arr.size();
        int i = 0;

        while (i < len) {
            if (arr.get(i) == t) {
                return i;
            }
            else {
                i = i + 1;
            }
        }
        return -1;
    }
    public static int[] sort(List<Integer> arr){
        int[] result = new int[arr.size()];
        for (int i = 0; i < arr.size(); i++)
        {
            for (int j = i + 1; j < arr.size(); j++)
            {
                int tmp = 0;
                if (arr.get(i) > arr.get(j))
                {
                    tmp = arr.get(i);
                    arr.set(i, arr.get(j));
                    arr.set(j,tmp);
                }}
        result[i]=arr.get(i);
        }
        return result;
    }
}
